//
// Created by joppe on 8-10-2018.

#include "dateTime.h"
#include "dateTimeExceptions.h"

#define ERROR 4
#define EQUILATERAL 3
#define ISOSCELES 2
#define SCALENE 1


dateTime::dateTime(int day, int month, int year)
{
    if (day > 0 && day <= 31)
    {
        this->day = day;
    }
    else
    {
        throw invalidDay;
    }
    if (month > 0 && month <= 12)
    {

        this->month = month;
    }
    else
    {
        throw invalidMonth;
    }
    if (year > 0)
    {
        this->year = year;
    }
    else
    {
        throw invalidYear;
    }

}

void dateTime::incrementDay(int day)
{
    int daysAfterIncrement = this->day + day;
    if (daysAfterIncrement > 0 && daysAfterIncrement <= 31)
    {
        this->day = daysAfterIncrement;

    }
    else
    {
        throw invalidDay;
    }
}

int dateTime::getMonth() const
{
    return month;
}

void dateTime::incrementMonth(int month)
{
    int monthsAfterIncrement =this->month + month;
    if (monthsAfterIncrement > 0 && monthsAfterIncrement <= 12)
    {
        this->month = this->month + month;
    }
    else
    {
        throw invalidMonth;
    }
}

int dateTime::getYear() const
{
    return year;
}

void dateTime::incrementYear(int year)
{
    if ((this->year + year) > 0)
    {
        this->year = this->year + year;
    }
    else
    {
        throw invalidYear;
    }
}


int dateTime::getDay()
{
    return this->day;
}
/*
std::string dateTime::printDateTime()
{
    std::ostream output;
    output << this->day << ":" << this->month << ":" << this->year;
}
 */

dateTime::~dateTime()
{

}

//source https://stackoverflow.com/questions/20097110/test-a-function-that-returns-triangle-type