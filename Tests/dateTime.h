//
// Created by joppe on 8-10-2018.
//

#ifndef SOFTWARE_ENGINEERING_TASK_3_TRIANGLE_TRIANGLE_H
#define SOFTWARE_ENGINEERING_TASK_3_TRIANGLE_TRIANGLE_H

#include <iostream>
#include <math.h>       /* pow */

class dateTime
{
public:
    dateTime(int day, int month, int year);
    int getDay();
    void incrementDay(int day);
    int getMonth() const;
    void incrementMonth(int month);
    int getYear() const;
    void incrementYear(int year);
    //std::string printDateTime();
    ~dateTime();
private:
    int day;
    int month;
    int year;
};

#endif //SOFTWARE_ENGINEERING_TASK_3_TRIANGLE_TRIANGLE_H
