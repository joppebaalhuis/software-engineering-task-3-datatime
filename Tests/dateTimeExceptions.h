//
// Created by joppe on 9-10-2018.
//

#ifndef SOFTWARE_ENGINEERING_TASK_3_DATETIME_EXCEPTIONS_H
#define SOFTWARE_ENGINEERING_TASK_3_DATETIME_EXCEPTIONS_H

#include <exception>

class invalidDay: public std::exception
{
    virtual const char* what() const throw()
    {
        return "invalid day";
    }
} invalidDay;


class invalidMonth: public std::exception
{
    virtual const char* what() const throw()
    {
        return "invalid month";
    }
} invalidMonth;

class invalidYear: public std::exception
{
    virtual const char* what() const throw()
    {
        return "invalid year";
    }
} invalidYear;


#endif //SOFTWARE_ENGINEERING_TASK_3_DATETIME_EXCEPTIONS_H
