//
// Created by joppe on 2-10-2018.
//

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "dateTime.h"
#include <exception>

#define ERROR 4
#define EQUILATERAL 3
#define ISOSCELES 2
#define SCALENE 1

using testing::Eq;

namespace dateTimeTest
{


    std::vector<int> boundaryTestValues;

    void CreateBounderyTestValues()
    {
        boundaryTestValues.push_back(20);
        boundaryTestValues.push_back(-16);
        boundaryTestValues.push_back(3);
        boundaryTestValues.push_back(-17);

    }

    TEST(test0, setup)
    {
        CreateBounderyTestValues();

    }
    TEST(test1, boundaryTestDays)
    {
      // int  value = -17;
      for (const int &value : dateTimeTest::boundaryTestValues)
        {
            dateTime dateTimeObj(5, 6, 2018);                                  /*create new object for each test*/
            try
            {
                dateTimeObj.incrementDay(value);
                if (dateTimeObj.getDay() > 31 || dateTimeObj.getDay() < 0)       //when there is no exception raised when i should be fail the test
                {
                    FAIL() << "expected invalid day";

                }
            }
            catch (std::exception &e)
            {

                EXPECT_EQ(e.what(), std::string("invalid day"));
                std::cout << value << "\t" << e.what() << "\texception raised as should be" << std::endl ;
                if (dateTimeObj.getDay() + value < 31 && dateTimeObj.getDay() +value > 0)            //if an exception is raised when there shouldn't be an exception fail the test
                {
                    FAIL() << "excepion raised on valid input";
                }
            }
        }

    }

    TEST(test2, boundaryTestMonth)
    {
        for (const int &value : dateTimeTest::boundaryTestValues)
        {
            dateTime dateTimeObj(15, 6, 2018);
            try
            {
                dateTimeObj.incrementMonth(value);
                if (dateTimeObj.getMonth()  > 12 || dateTimeObj.getDay()  < 0)       //when there is no exception raised when i should be fail the test
                {
                    FAIL() << "expected invalid month";
                }
            }
            catch (std::exception &e)
            {

                EXPECT_EQ(e.what(), std::string("invalid month"));
                std::cout << value << "\t" << e.what() << "\texception raised as should be" << std::endl ;
                if (dateTimeObj.getMonth() + value < 12 && dateTimeObj.getMonth() + value > 0)            //if an exception is raised when there shouldn't be an exception fail the test
                {                           // ^                                    ^
                                            // need increment with the test value because the tested methode did not set the new value
                    FAIL() << "excepion raised on valid input";
                }
            }
        }
    }


    TEST(test3, boundaryTestYear)
    {
        for (const int &value : dateTimeTest::boundaryTestValues)
        {
            dateTime dateTimeObj(15, 5, 2018);
            try
            {
                dateTimeObj.incrementYear(10);
                if (dateTimeObj.getYear() < 0)
                {
                    FAIL() <<  "expected invalid yeaer";
                }
            }
            catch (std::exception &e)
            {
                if (dateTimeObj.getYear() + value < 0)
                {
                    FAIL() << "excepion raised on valid input";
                }
                EXPECT_EQ(e.what(), std::string("invalid year"));
                std::cout << value << "\t" << e.what() << "\texception raised as should be" << std::endl ;

            }
        }
    }

}

